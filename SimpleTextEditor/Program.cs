﻿/*
 * Created by SharpDevelop.
 * User: beppe
 * Date: 18/04/2016
 * Time: 21:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace SimpleTextEditor
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		
		static TextFileModel model;
		static TextFileController controller;
		
		public static TextFileModel Model{
			get{ return model; }
		}
		
		public static TextFileController Controller{
			get{ return controller; }
		}
		
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			model = new TextFileModel();
			controller = new TextFileController(model);
			
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
		
	}
}
