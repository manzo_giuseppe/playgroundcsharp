﻿/*
 * Created by SharpDevelop.
 * User: beppe
 * Date: 18/04/2016
 * Time: 22:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace SimpleTextEditor
{
	/// <summary>
	/// Description of TextFileController.
	/// </summary>
	public class TextFileController
	{
		private TextFileModel model;
		
		public TextFileController(TextFileModel model)
		{
			this.model = model;
		}
		
		public void open(string path){
			model.openFile(path);
			model.loadContent();
		}
		
		public void close(){
			model.closeFile();
		}
		
		public void replaceContent(string [] lines){
			model.storeContent(lines);
		}
	}
}
