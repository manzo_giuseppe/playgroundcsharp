﻿/*
 * Created by SharpDevelop.
 * User: beppe
 * Date: 18/04/2016
 * Time: 21:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace SimpleTextEditor
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			Program.Model.ContentChangedEvent += OnContentChanged;
		}
		
		void OpenFileButtonClick(object sender, EventArgs e)
		{
			OpenFileDialog d = new OpenFileDialog();
			if( d.ShowDialog() == DialogResult.OK ){
				Program.Controller.open(d.FileName);
				filePathLabel.Text = d.SafeFileName;
			}
		}
		
		void OnContentChanged(object sender, EventArgs args){
			contentBox.Lines = Program.Model.Content;
		}
		void SaveFileButtonClick(object sender, EventArgs e)
		{
			Program.Controller.replaceContent(contentBox.Lines);
		}
	}
}
