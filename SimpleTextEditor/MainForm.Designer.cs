﻿/*
 * Created by SharpDevelop.
 * User: beppe
 * Date: 18/04/2016
 * Time: 21:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SimpleTextEditor
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button openFileButton;
		private System.Windows.Forms.Button newFileButton;
		private System.Windows.Forms.Label filePathLabel;
		private System.Windows.Forms.TextBox contentBox;
		private System.Windows.Forms.Button saveFileButton;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.openFileButton = new System.Windows.Forms.Button();
			this.newFileButton = new System.Windows.Forms.Button();
			this.filePathLabel = new System.Windows.Forms.Label();
			this.contentBox = new System.Windows.Forms.TextBox();
			this.saveFileButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// openFileButton
			// 
			this.openFileButton.Location = new System.Drawing.Point(13, 13);
			this.openFileButton.Name = "openFileButton";
			this.openFileButton.Size = new System.Drawing.Size(75, 23);
			this.openFileButton.TabIndex = 0;
			this.openFileButton.Text = "Open File";
			this.openFileButton.UseVisualStyleBackColor = true;
			this.openFileButton.Click += new System.EventHandler(this.OpenFileButtonClick);
			// 
			// newFileButton
			// 
			this.newFileButton.Location = new System.Drawing.Point(94, 12);
			this.newFileButton.Name = "newFileButton";
			this.newFileButton.Size = new System.Drawing.Size(75, 23);
			this.newFileButton.TabIndex = 1;
			this.newFileButton.Text = "New File";
			this.newFileButton.UseVisualStyleBackColor = true;
			// 
			// filePathLabel
			// 
			this.filePathLabel.Location = new System.Drawing.Point(176, 13);
			this.filePathLabel.Name = "filePathLabel";
			this.filePathLabel.Size = new System.Drawing.Size(405, 23);
			this.filePathLabel.TabIndex = 2;
			this.filePathLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// contentBox
			// 
			this.contentBox.Location = new System.Drawing.Point(13, 43);
			this.contentBox.Multiline = true;
			this.contentBox.Name = "contentBox";
			this.contentBox.Size = new System.Drawing.Size(568, 354);
			this.contentBox.TabIndex = 3;
			// 
			// saveFileButton
			// 
			this.saveFileButton.Location = new System.Drawing.Point(506, 403);
			this.saveFileButton.Name = "saveFileButton";
			this.saveFileButton.Size = new System.Drawing.Size(75, 23);
			this.saveFileButton.TabIndex = 4;
			this.saveFileButton.Text = "Save File";
			this.saveFileButton.UseVisualStyleBackColor = true;
			this.saveFileButton.Click += new System.EventHandler(this.SaveFileButtonClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(593, 438);
			this.Controls.Add(this.saveFileButton);
			this.Controls.Add(this.contentBox);
			this.Controls.Add(this.filePathLabel);
			this.Controls.Add(this.newFileButton);
			this.Controls.Add(this.openFileButton);
			this.Name = "MainForm";
			this.Text = "SimpleTextEditor";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
