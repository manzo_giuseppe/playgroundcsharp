﻿/*
 * Created by SharpDevelop.
 * User: beppe
 * Date: 18/04/2016
 * Time: 21:15
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Collections;
namespace SimpleTextEditor
{
	/// <summary>
	/// Description of TextFileModel.
	/// </summary>
	public class TextFileModel
	{
		
		FileStream stream;
		bool isOpened;
		string [] content = new string[0];
		
		public bool IsOpened{
			get{ return isOpened; }
		}
		
		public string [] Content{
			get{ return content; }
		}
		
		public delegate void FileOpened(object sender, EventArgs args);
		public event FileOpened FileOpenedEvent = delegate{};
		
		public delegate void FileClosed(object sender, EventArgs args);
		public event FileOpened FileClosedEvent = delegate{};
		
		public delegate void ContentChanged(object sender, EventArgs args);
		public event ContentChanged ContentChangedEvent = delegate{};
		
		public TextFileModel()
		{
		}
		
		public void openFile(string path){
			if(stream != null){
				stream.Flush();
				stream.Close();
			}
			stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
			isOpened = true;
			FileOpenedEvent(this, EventArgs.Empty);
		}
		
		public void loadContent(){
			if(isOpened){
				Array.Clear(content, 0, content.Length);
				stream.Seek(0, SeekOrigin.Begin);
				var reader = new StreamReader(stream);
				ArrayList lines = new ArrayList();
				String c;
				while( (c=reader.ReadLine()) != null ){
					lines.Add(c);
				}
				content = (string[])lines.ToArray(typeof(string));
				lines.Clear();
				ContentChangedEvent(this, EventArgs.Empty);
			}
		}
		
		public void storeContent(string [] lines){
			if(isOpened){
				stream.Seek(0, SeekOrigin.Begin);
				var writer = new StreamWriter(stream);
				foreach( string l in lines ){
					writer.WriteLine(l);
				}
				content = new string[lines.Length];
				Array.Copy(lines, content, lines.Length);
				//ContentChangedEvent(this, EventArgs.Empty);
			}
		}
		
		public void closeFile(){
			if(stream != null){
				stream.Close();
				stream = null;
				isOpened = false;
				FileClosedEvent(this, EventArgs.Empty);
			}
		}
		
		
		
	}
}
